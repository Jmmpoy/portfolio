import React, { Component } from 'react';

import './App.css';
import Social from './components/social';
import Links from './components/links';
import FadeIn from 'react-fade-in';

class App extends Component {
constructor(props){
  super(props)
  this.state={
    isOpen: false
  }
}
  
  render() {
    return (
      <FadeIn delay={600}>
        <div className=" container App "> 
          <Links/>
          <div className="row ">
            
            <div className="col mx-auto">
              <p className="last mx-auto">Mpoy Jean-Marc</p>   
              <p className="job mx-auto"><mark>Web</mark>Developer</p>
            </div>
            
          </div>
          <Social delay={1700}/>
      </div>
      </FadeIn>
    );
  }
}

export default App;


