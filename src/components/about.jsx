import React from 'react'
import FadeIn from 'react-fade-in/lib/FadeIn'
import Social from './social';
import Links from './links';
import { Map, GoogleApiWrapper } from 'google-maps-react';

function about() {
    return (
        <FadeIn delay={300}>
            <div className="container ">
            <Links/>
                <div className="row  box">
                    <div className="col-6 about">
                        <h4 className="text-center pb-5">Qui Suis-je ?</h4>
                    <p className="text-center ">     
                    Diplômée d'une Licence en Communication Option Médias Numériques ainsi que d'un  Bac +2  Développeur Web/ Web Mobile.
                    Je me suis spécialisée dans le développement Web afin de stimuler ma curiosité et mon goût pour le design et la technologie.
                    </p>
                    </div>  
                </div>
               <Social/>
            </div>
        </FadeIn>
    )
}

export default about
