import React from 'react'
import { Link} from 'react-router-dom'

function links() {
    return (
        <div className="row d-flex flex-row justify-content-around">
          <Link to="/">
            <button>Home</button>
          </Link>
          <Link to="./about"><button>About</button></Link>
          <Link to="./projects"><button>Projects</button></Link>
        </div>
    )
}

export default links
