import React from 'react'
import FadeIn from 'react-fade-in/lib/FadeIn'
import Social from './social';
import Links from './links';

function projects() {
    return (
        <FadeIn delay={600}> 
            <div className="container projects">
            <Links/>
                <div className="row box m-5">
                    <div className="col-sm-12 col-md-4 projects bg-danger"></div>
                    <div className="ccol-sm-12 col-md-4  projects bg-info"></div>
                    <div className="col-sm-12 col-md-4 projects bg-warning"></div>    
                </div>  
                <Social delay={1700}/>
            </div>     
        </FadeIn>
    )
}

export default projects
