import React from 'react'

function social() {
    return (
        <div className="row social ">
          <ul className="social-media-list">
                
                <li>
                    <a href="https://twitter.com/">
                        <ion-icon class="logo-twitter" name="logo-twitter"></ion-icon>
                    </a>
                </li>
                
                <li>
                    <a href="https://github.com/">
                        <ion-icon class="logo-github" name="logo-github"></ion-icon>
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default social
